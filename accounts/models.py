import os
from django.core.validators import MaxValueValidator, MinValueValidator
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin 
from django.contrib.auth.base_user import BaseUserManager
from django.db import models

# Create your models here.

class UsuarioManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, matricula, password, **extra_fields):
        """
        Create and save a user with the given username, email, and password.
        """
        if not matricula:
            raise ValueError('Se debe definir una matricula.')
        matricula = self.model.normalize_username(matricula)
        user = self.model(matricula=matricula, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, matricula, password=None, **extra_fields):
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(matricula, password, **extra_fields)

    def create_superuser(self, matricula, password, **extra_fields):
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser debe tener su valor is_superuser=True')

        return self._create_user(matricula, password, **extra_fields)

class Usuario(AbstractBaseUser, PermissionsMixin):
    username = None
    matricula = models.CharField(max_length=10, blank=False, primary_key=True, unique=True)
    nombre = models.CharField(max_length=20, blank=False)
    ap_pat = models.CharField(max_length=20, blank=True, null=True)
    ap_mat = models.CharField(max_length=20, blank=True, null=True)
    sexo = models.CharField(max_length=2, blank=True, null=True)
    fecha_nacimiento = models.DateField()
    correo = models.EmailField(blank=True, null=True)
    telefono = models.CharField(max_length=30, blank=True, null=True)
    celular = models.CharField(max_length=30, blank=True, null=True )
    generacion = models.CharField(max_length=20, blank=True, null=True)
    carrera = models.CharField(max_length=30, blank=True, null=True)
    
    titulado = models.BooleanField(default=True)
    ingles = models.IntegerField(blank=True, null=True)
    



    objects = UsuarioManager()
    
    USERNAME_FIELD = 'matricula'
    REQUIRED_FIELDS = ['nombre']

##crear un usuario de estudiante, otro de maestro y otro de pariente/tutor

class UserInfo(models.Model):
    CALIF = [
        (5, 'Excelente'),
        (4, 'Muy Bueno'),
        (3, 'Regular'),
        (2, 'Malo'),
        (1, 'Muy Malo')
    ]

    ESTUDIOS = [
        ('Licenciatura'),
        ('Maestria'),
        ('Doctorado'),
        ('Especialidad'),
        ('Diplomado')
    ]

    ACTIVIDADES = [
        ('Trabajo'),
        ('Estudio'),
        ('Ambas'),
        ('Ninguna')
    ]

    MEDIOS_EMPLEO = [
        'Residencia profesional',
        'Medios Masivos de Comunicación',
        'Redes Sociales',
        'Internet',
        'Bolsa de Trabajo ITH',
        'Contactos personales',
        'Auto empleo',
        'Mi propia busqueda',
        'Recomendación',
        'No trabaja'
    ]

    ANTIGUEDAD = [
        '1 año',
        '2 años',
        '3 años',
        '4 años',
        '5 años',
        'Entre 5 y 10 años',
        'Entre 10 y 15 años',
        'Entre 15 y 20 años',
        'Más de 20 años'
    ]

    NIVELES = [
        'Técnico',
        'Supervisor',
        'Funcionario',
        'Desarrollador',
        'Jefe de área',
        'Gerente',
        'Directivo',
        'Ingeniero',
        'Administrativo',
        'Otro (especificar cual)'
    ]

    CONDICIONES = [
        'Base',
        'Confianza',
        'Eventual'
    ]

    ORGANISMOS = [
        'Público',
        'Privado'
    ]

    TIPOS_SECTOR = [
        ('Consultoría logística y tecnología','Consultoría logística y tecnología'),
        ('Tecnológico','Tecnológico'),
        ('Sector primario','Sector primario (Pesca, Ganadería, agricultura)'),
        ('Sector secundario','Sector secundario (Manufactura, transformación)'),
        ('Sector terciario','Sector terciario (Servicios)'),
    ]

    TAMANOS_EMPRESA = [
        ('Microempresa', 'Microempresa (1-30)'),
        ('Pequeña', 'Pequeña (31-100)'),
        ('Mediana', 'Mediana (101-500)'),
        ('Grande', 'Grande (más de 500)'),
    ]

    EFICIENCIA = [
        'Muy eficiente',
        'Eficiente',
        'Poco eficiente',
        'Deficiente'
    ]

    user = models.OneToOneField(Usuario, on_delete=models.CASCADE)
    #medios y recursos para el parendizaje
    calidad_docentes = models.CharField(max_length=20, blank=False, null=True, choices=CALIF)
    opor_investigacion = models.CharField(max_length=20, blank=False, null=True, choices=CALIF)
    enfasis = models.CharField(max_length=20, blank=False, null=True, choices=CALIF)
    condiciones = models.CharField(max_length=20, blank=False, null=True, choices=CALIF)
    residencia = models.CharField(max_length=20, blank=False, null=True, choices=CALIF)
    #estudios (en caso de seguir estudiando)
    actividad = models.CharField(max_length=20, blank=False, null=True, choices=ACTIVIDADES)
    lic = models.BooleanField(default=True)
    maestria = models.BooleanField(default=False)
    doctorado = models.BooleanField(default=False)
    especialidad = models.BooleanField(default=False)
    diplomado = models.BooleanField(default=False)
    institucion = models.CharField(max_length=30, blank=True, null=True)

    #Tiempo transcurrido para obtener el primer empleo
    tiempo_trabajo= models.CharField(max_length=30, blank=True, null=True)
    #medio para obtener el empleo
    medio_trabajo= models.CharField(max_length=30, blank=True, null=True, choices=MEDIOS_EMPLEO)
    #requisitos de contratacion (opcion multiple)
    competencias = models.BooleanField(default=False)
    examen = models.models.BooleanField(default=False)
    act_hab = models.BooleanField(default=False)
    titulo = models.BooleanField(default=False)
    idioma = models.BooleanField(default=False)
    personal = models.BooleanField(default=False)
    equipo = models.BooleanField(default=False)
    presion = models.BooleanField(default=False)
    ninguno = models.BooleanField(default=False)
    software = models.CharField(max_length=30, blank=True, null=True)
    otro_req = models.CharField(max_length=30, blank=True, null=True)
    #idioma para:
    idioma_hablar = models.CharField(max_length=30, blank=True, null=True)
    hablar_por = models.DecimalField(decimal_places=1, blank=True, null=True)
    idioma_leer = models.CharField(max_length=30, blank=True, null=True)
    leer_por = models.DecimalField(decimal_places=1, blank=True, null=True)
    idioma_escribir = models.CharField(max_length=30, blank=True, null=True)
    escribir_por = models.DecimalField(decimal_places=1, blank=True, null=True)
    idioma_escuchar = models.CharField(max_length=30, blank=True, null=True)
    escuchar_por = models.DecimalField(decimal_places=1, blank=True, null=True)
    #antiguedad en el empleo
    antiguedad_empleo = models.CharField(max_length=30, blank=True, null=True, choices=ANTIGUEDAD)
    #FECHA DE INGRESO al empleo
    fecha_ingreso_empleo = models.DateField(blank=True, null=True)
    salario_minimo = models.DecimalField(blank=True, null=True, default=0.00, decimal_places=2, validators=[MaxValueValidator(1000), MinValueValidator(0)])
    nivel_jerarquico = models.CharField(max_length=30, blank=True, null=True, choices=NIVELES)
    conficion_trabajo = models.CharField(max_length=30, blank=True, null=True, choices=CONDICIONES)
    #relacion del trabajo con area de formacion
    rel_trabajo = models.IntegerField(blank=True, null=True,  validators=[MaxValueValidator(100), MinValueValidator(0)])
    organismo = models.CharField(max_length=10, blank=True, null=True, choices=ORGANISMOS)
    giro_principal = models.CharField(max_length=250, blank=True, null=True)

    #SECTOR ECONOMICO
    sector_economico= models.CharField(max_length=10, blank=True, null=True, choices=TIPOS_SECTOR)
    tamano_empresa = models.CharField(max_length=25, blank=True, null=True, choices=TIPOS_SECTOR)
    eficiencia = models.CharField(max_length=25, blank=True, null=True, choices=EFICIENCIA)


