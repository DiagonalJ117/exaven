from django.urls import path, include
from . import views
from django.contrib.auth.models import User

app_name = 'accounts'

urlpatterns = [
    path('<str:matricula>', views.account, name="account"),
    path('login/', views.login_view, name="login"),
    path('signup/', views.signup_view, name="signup"),
    path('logout/', views.logout_view, name="logout"),
    path('update/', views.account_update, name="account_update")

]