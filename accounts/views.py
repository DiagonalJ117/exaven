from django.http import HttpResponse
from django.shortcuts import redirect
from django.shortcuts import render
from django.contrib import messages
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from .forms import Signup_UsuarioForm, UsuarioProfileForm
from django.contrib.auth import login, logout
from django.contrib.auth.models import Permission, User
from .models import Usuario


# Create your views here.
def signup_view(request):
    if request.method=='POST':
        form = Signup_UsuarioForm(request.POST)
        if form.is_valid():
            #permission = Permission.objects.get(name='is_staff')
            
            user = form.save()
            user.save()
            #user.set_permissions.add(permission)
            #login(request, user)
            messages.success(request, 'Cuenta creada!')
            return redirect('accounts:signup') #HttpResponse('Success!')
    else:
        
        form = Signup_UsuarioForm()

    return render(request, 'signup.html', {'form': form})
    

def login_view(request):
    if request.method=='POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            if 'next' in request.POST:
                return redirect(request.POST.get('next'))
            else:
                #return redirect('accounts:account', request.user.username)
                return redirect('home')
        else:
            messages.error(request, "Nombre de Usuario o contraseña incorrecta.")
    else:
        form = AuthenticationForm()

    return render(request, 'login.html', {'form': form})

def account(request, matricula):
    user = Usuario.objects.get(matricula=matricula)
    if request.method == 'POST':
        form = UsuarioProfileForm(data=request.POST, instance=request.user)
        update = form.save(commit=False)
        update.user = request.user.matricula
        update.save()
        return redirect('accounts:account', request.user)
    else:
        form = UsuarioProfileForm(instance=request.user)
    return render(request, 'account.html', {'user':user, 'form': form})

def account_update(request):
    if request.method == 'POST':
        form = UsuarioProfileForm(data=request.POST, instance=request.user)
        update = form.save(commit=False)
        update.user = request.user.matricula
        update.save()
        return redirect('accounts:account', request.user)
    else:
        form = UsuarioProfileForm(instance=request.user)

    return render(request, 'account.html', {'form': form})
    

def logout_view(request):
    if request.method == 'POST':
        logout(request)
        return redirect('accounts:login')
    else:
        return HttpResponse('Not a POST request')
    


