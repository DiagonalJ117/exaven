from django.db import models
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms
from django.core.exceptions import ValidationError
from accounts.models import Usuario
from django.core.validators import MinValueValidator

class Signup_UsuarioForm(UserCreationForm):
    required_css_class='required'
    matricula = forms.IntegerField(label='Matricula', required=True, validators = [MinValueValidator(0, message='No se aceptan valores negativos.')])
    nombre = forms.CharField(label='Nombre', max_length=30, required=True)
    ap_pat = forms.CharField(label='Apellido Paterno', max_length=30, required=True)
    ap_mat = forms.CharField(label='Apellido Materno', max_length=30, required=True)
    correo = forms.EmailField(label='Correo', required=False)
    generacion = forms.CharField(label='Generacion', max_length=30)
    carrera = forms.CharField(label='Carrera', max_length=30)
    password1 = forms.CharField(label='Contraseña', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Confirmar Contraseña', widget=forms.PasswordInput)

    def clean_matricula(self):
        matricula = self.cleaned_data['matricula']
        r = Usuario.objects.filter(matricula=matricula)
        if r.count():
            raise ValidationError("Matricula ya existente.")
        return matricula

    def clean_correo(self):
        correo = self.cleaned_data['correo']
        r = Usuario.objects.filter(correo=correo)
        if r.count():
            raise ValidationError("Cuenta con este correo ya existente.")
        return correo


    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password1 and password2 and password1 != password2:
            raise ValidationError("Contraseñas no coinciden.")

        return password2

    def save(self, commit=True):
        user = super(Signup_UsuarioForm, self).save(commit=False)
        user = Usuario.objects.create_user(
            
            self.cleaned_data['matricula'],
            self.cleaned_data['password1'],
        )
        #user.matricula = self.cleaned_data.get('matricula')
        user.nombre = self.cleaned_data.get('nombre')
        if user.correo == True:
            user.correo = self.cleaned_data.get('correo')
        user.ap_pat = self.cleaned_data.get('ap_pat')
        user.ap_mat = self.cleaned_data.get('ap_mat')
        user.carrera = self.cleaned_data.get('carrera')
        user.generacion = self.cleaned_data.get('generacion')
        if commit:
            user.save()

        return user

    class Meta:
        model = Usuario
        fields = (
            'matricula',
            'correo',
            'nombre',
            'ap_pat',
            'ap_mat',
            'carrera',
            'generacion',
            'telefono',
            'password1',
            'password2'
        )

class UsuarioProfileForm(forms.ModelForm):
    class Meta:
        model = Usuario
        fields = ['matricula', 'nombre', 'ap_pat', 'ap_mat', 'correo', 'telefono', 'generacion', 'carrera']
        labels = {
            'matricula': 'Matricula',
            'nombre': 'Nombre',
            'ap_mat': 'Apellido Materno',
            'ap_pat': 'Apellido Paterno',
            'correo': 'Correo Electronico',
            'telefono': 'Telefono',
            'carrera': 'Carrera',
            'generacion': 'Generación'
        }