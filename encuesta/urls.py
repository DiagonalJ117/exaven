from django.urls import path
from encuesta import views
from django.contrib.auth.models import User

app_name = 'encuesta'

urlpatterns = [
    path('', views.encuesta_main, name="encuesta-home"),

]