from django.urls import path

from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static
from django.conf import settings
from bolsa_trabajo import views

app_name='bolsa_trabajo'

urlpatterns = [
    path('', views.bt_main, name="bt-home"),
    path('empresa_add/', views.empresa_add, name="empresa-add"),
    path('<str:id>/empresa_delete', views.empresa_delete, name="empresa-delete"),
    path('<str:id>/empresa_edit', views.empresa_edit, name="empresa-edit"),
    path('<str:id>', views.empresa_detail, name="empresa-detail"),
    path('<str:id>/contacto_add', views.empresa_contacto_add, name="contacto-add"),
    path('contacto_delete/<str:id>/', views.empresa_contacto_delete, name="contacto-delete")

]




urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)