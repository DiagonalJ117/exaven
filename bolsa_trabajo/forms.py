from django import forms
from bolsa_trabajo import models

class AddEmpresaForm(forms.ModelForm):
    required_css_class = 'required'
    def clean(self):
        telefono = self.cleaned_data.get('telefono')
    
    class Meta:
        model = models.Empresa
        fields = [ 'nombre', 'telefono', 'codigo_postal', 'calle', 'colonia', 'numero', 'ciudad', 'estado', 'pais' ]
        labels = {
            'nombre': 'Nombre',
            'telefono': 'Teléfono',
            'codigo_postal': 'Código Postal',
            'calle': 'Calle',
            'colonia': 'Colonia',
            'numero': 'Número',
            'ciudad': 'Ciudad',
            'estado': 'Estado',
            'pais': 'País'
        }

class AddContactoForm(forms.ModelForm):
    required_css_class = 'required'
    def clean(self):
        email = self.cleaned_data.get('email')
        telefono = self.cleaned_data.get('telefono')

        if not email and not telefono:
            msg = forms.ValidationError("Especificar telefono o correo electronico")
            self.add_error('email', msg)
            self.add_error('telefono', msg)
            raise msg
    
    class Meta:
        model = models.Contacto_Empresa
        fields = [ 'empresa', 'nombre', 'telefono', 'email']