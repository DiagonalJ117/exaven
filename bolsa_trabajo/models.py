import os
from django.db import models

# Create your models here.


class Empresa(models.Model):
    nombre = models.CharField(max_length=30, blank=False)
    telefono = models.CharField(max_length=20, blank=True, null=True)
    codigo_postal = models.PositiveIntegerField(blank=True, null=True)
    calle = models.CharField(max_length=20, blank=True, null=True)
    colonia = models.CharField(max_length=20, blank=True, null=True)
    numero = models.SmallIntegerField(blank=True, null=True)
    ciudad = models.CharField(max_length=15, blank=True, null=True)
    estado = models.CharField(max_length=15, blank=True, null=True)
    pais = models.CharField(max_length=15, blank=True, null=True)

class Contacto_Empresa(models.Model):
    empresa = models.ForeignKey(Empresa, default=None, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=30, blank=False)
    telefono = models.CharField(max_length=20, blank=True, null=True)
    email = models.EmailField(blank=True, null=True)