from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse
from django.core.exceptions import ValidationError
from django.contrib import messages
from .models import Empresa, Contacto_Empresa
from bolsa_trabajo import forms

# Create your views here.


def bt_main(request):
    empresas = Empresa.objects.all().order_by('-nombre')
    contacto = Contacto_Empresa.objects.all().order_by('-id').first()

    return render(request, 'bt_home.html', {'empresas':empresas, 'contacto':contacto})

def empresa_detail(request, id):
    
    empresa = Empresa.objects.get(id=id)
    empresa = get_object_or_404(Empresa, id=id)
    
    contactos = Contacto_Empresa.objects.all().filter(empresa_id=id).order_by("-id")
    

    return render(request, 'empresa_detail.html', {'empresa':empresa, 'contactos':contactos})

def empresa_add(request):
    if request.method == 'POST':
        form = forms.AddEmpresaForm(request.POST)
        if form.is_valid():
            #guardar empresa a la base de datos
            instance = form.save(commit=False)
            instance.save()
            messages.success(request, 'Empresa Registrada Correctamente')
            return redirect('bolsa_trabajo:bt-home')
        else:
            messages.warning(request, 'Error al Registrar Empresa')
            return redirect('bolsa_trabajo:bt-home')
    else:
        form = forms.AddEmpresaForm()

    return render(request, 'bt_home.html', {'form': form})

   

def empresa_edit(request, id):
    empresa = Empresa.objects.get(id=id)
    if request.method == 'POST':
        form = forms.AddEmpresaForm(request.POST or None, instance=empresa)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.save()
            messages.success(request, 'Informacion Actualizada')
            return redirect('bolsa_trabajo:empresa-detail', empresa.id)

        else:
            messages.warning(request, 'Error al Actualizar')
            return redirect('bolsa_trabajo:empresa-detail', empresa.id)
    else:
        form = forms.AddEmpresaForm(instance=empresa)
    
    return render(request, 'empresa_detail.html', {'empresa':empresa, 'form':form})

def empresa_delete(request,id):
    contactos = Contacto_Empresa.objects.all().filter(empresa_id=id)
    empresa = Empresa.objects.get(id=id)
    contactos.delete()
    empresa.delete()
    messages.info(request,'Empresa eliminada')
    return redirect('bolsa_trabajo:bt-home')
    

def empresa_contacto_add(request, id):
    empresa = Empresa.objects.get(id=id)
    if request.method == 'POST':
        form = forms.AddContactoForm(request.POST)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.save()
            messages.success(request, 'Contacto registrado correctamente')
        
            return redirect('bolsa_trabajo:empresa-detail', empresa.id)
        else:
            messages.warning(request, 'Error al Registrar Contacto')
            return redirect('bolsa_trabajo:empresa-detail', empresa.id)
    return redirect('bolsa_trabajo:empresa-detail', empresa.id)

def empresa_contacto_edit(request):
    
    return HttpResponse('contactoEdit')

def empresa_contacto_delete(request, id):
    
    contacto = Contacto_Empresa.objects.get(id=id)
    empresa = Empresa.objects.get(id=contacto.empresa_id)
    contacto.delete()
    messages.info(request, 'Contacto Eliminado')

    return redirect('bolsa_trabajo:empresa-detail', empresa.id)