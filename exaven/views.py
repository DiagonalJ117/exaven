from django.http import HttpResponse
from django.shortcuts import render
from django.shortcuts import redirect

def homepage(request):
    return render(request, 'home.html')

def edu_continua(request):
    return render(request, 'edu_continua.html')

def titulacion(request):
    return render(request, 'titulacion.html')